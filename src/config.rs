use std::collections::HashMap;
use std::error::Error;

use crate::task::Task;

#[derive(Debug)]
pub struct Config {
    pub tasks: HashMap<String, Task>,
}

impl Config {
    pub fn new(s: &str) -> Result<Self, Box<dyn Error>> {
        let tasks = toml::from_str(&s)?;

        Ok(Self { tasks })
    }
}
