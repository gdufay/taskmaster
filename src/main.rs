use std::env;
use std::process;

use taskmaster::cli::Cli;

fn main() {
    let args: Vec<String> = env::args().collect();

    let cli = Cli::new(&args).unwrap_or_else(|e| {
        eprintln!("{}", e);
        process::exit(1);
    });

    if let Err(e) = taskmaster::run(cli) {
        eprintln!("Error running application: {}", e);
        process::exit(1);
    }
}
