use std::error::Error;

use crate::shell::Shell;

pub fn status(shell: &Shell, cmd: &str, args: &[&str]) -> Result<(), Box<dyn Error>> {
    if args.is_empty() {
        usage(cmd);
        return Ok(());
    }

    shell.tshell.send(format!("{} {}", cmd, args.join(" ")))?;
    println!("Daemon response {}", shell.rshell.recv()?);
    Ok(())
}

pub fn exit(shell: &Shell) -> Result<(), Box<dyn Error>> {
    shell.tshell.send("exit".to_string())?;

    Ok(())
}

pub fn usage(cmd: &str) {
    let s = match cmd {
        "status" => "status all | program ...",
        _ => "Print all usage here",
    };

    eprintln!("Usage: {}", s);
}
