use std::error::Error;
use std::io::BufRead;
use std::sync::mpsc::{Receiver, Sender};
use std::{fs, io};

use crate::config::Config;
use crate::shell_command;

pub type TShell = Sender<String>;
pub type RShell = Receiver<String>;

// config is for autocompletion
pub struct Shell<'a> {
    pub tshell: TShell,
    pub rshell: RShell,
    pub config_file: &'a str,
    pub config: Config,
}

impl<'a> Shell<'a> {
    pub fn new(
        tshell: TShell,
        rshell: RShell,
        config_file: &'a str,
    ) -> Result<Self, Box<dyn Error>> {
        let s = fs::read_to_string(config_file)?;
        let config = Config::new(&s)?;

        Ok(Self {
            tshell,
            rshell,
            config_file,
            config,
        })
    }

    pub fn run(&self) -> Result<(), Box<dyn Error>> {
        // TODO: line edition
        let input = io::stdin();

        for line in input.lock().lines() {
            let mut line = line?;

            line.make_ascii_lowercase();
            if line == "exit" {
                break;
            } else {
                self.dispatch(&line)?;
            }
        }

        shell_command::exit(&self)?;

        Ok(())
    }

    fn dispatch(&self, s: &str) -> Result<(), Box<dyn Error>> {
        let mut parsed_s = s.split_whitespace();
        let (cmd, args): (&str, Vec<&str>) = (parsed_s.next().unwrap_or(""), parsed_s.collect());

        match cmd {
            "status" => shell_command::status(&self, &cmd, &args)?,
            _ => shell_command::usage(""),
        }

        Ok(())
    }
}
