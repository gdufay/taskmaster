use std::path::{PathBuf};
use std::env;
use std::collections::HashMap;

use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Task {
    pub cmd: String,
    #[serde(default = "proc_default")]
    pub procs: u8,
    #[serde(default = "autostart_default")]
    pub autostart: bool,
    #[serde(default)]
    pub autorestart: AutoRestart,
    #[serde(default = "startretries_default")]
    pub startretries: usize,
    #[serde(default = "exitcodes_default")]
    pub exitcodes: Vec<i32>,
    #[serde(default = "startsecs_default")]
    pub startsecs: usize,
    #[serde(default)]
    pub restartretries: usize,
    // TODO later #[serde(default = "stopsignal_default")]
    #[serde(default)]
    pub stopsignal: i32,
    #[serde(default = "stopwaitsec_default")]
    pub stopwaitsec: usize,
    #[serde(default)]
    pub stdout_logfile: Option<PathBuf>,
    #[serde(default)]
    pub stderr_logfile: Option<PathBuf>,
    #[serde(default)]
    pub environnement: HashMap<String, String>,
    #[serde(default = "chdir_default")]
    pub chdir: PathBuf,
    #[serde(default = "umask_default")]
    pub umask: u32,
}

#[derive(Debug, Deserialize)]
pub enum AutoRestart {
    #[serde(rename = "true")]
    True,
    #[serde(rename = "false")]
    False,
    #[serde(rename = "unexpected")]
    Unexpected,
}

impl Default for AutoRestart {
    fn default() -> Self {
        Self::Unexpected
    }
}

fn proc_default() -> u8 {
    1
}

fn autostart_default() -> bool {
    true
}

fn startretries_default() -> usize {
    3
}

fn exitcodes_default() -> Vec<i32> {
    vec![0]
}

fn startsecs_default() -> usize {
    1
}

/*
fn stopsignal_default() -> {

}
*/

fn stopwaitsec_default() -> usize {
    10
}

fn chdir_default() -> PathBuf {
    env::current_dir().unwrap_or_default()
}

fn umask_default() -> u32 {
    0o22
}
