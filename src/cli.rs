// TODO: use reference

pub struct Cli {
    pub file: String,
}

fn usage() -> &'static str {
    "Usage: ./taskmaster config_file"
}

impl Cli {
    pub fn new(args: &[String]) -> Result<Self, &'static str> {
        if args.len() == 2 {
            Ok(Self {
                file: args[1].clone(),
            })
        } else {
            Err(usage())
        }
    }
}
