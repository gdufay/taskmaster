use std::error::Error;
use std::sync::mpsc;
use std::thread;

pub mod cli;
use crate::cli::Cli;

mod config;

pub mod shell_command;
pub mod task;

mod shell;
use crate::shell::Shell;

mod daemon;
use crate::daemon::Daemon;

pub fn run(cli: Cli) -> Result<(), Box<dyn Error>> {
    let (tdaemon, rshell) = mpsc::channel();
    let (tshell, rdaemon) = mpsc::channel();

    let daemon = Daemon::new(tdaemon, rdaemon, &cli.file)?;
    let handler = thread::Builder::new().spawn(move || {
        daemon.run();
    })?;

    let shell = Shell::new(tshell, rshell, &cli.file)?;
    shell.run()?;

    handler.join().expect("fix here"); // TODO

    Ok(())
}
