use std::error::Error;
use std::{fs,process};
use std::sync::mpsc::{Receiver, Sender,TryRecvError};
use std::process::{Command,Stdio};
use std::time::Instant;
use std::fs::{OpenOptions,File};
use std::path::Path;

use crate::config::Config;
use crate::task::Task;

pub type TDaemon = Sender<String>;
pub type RDaemon = Receiver<String>;

pub struct Daemon {
    tdaemon: TDaemon,
    rdaemon: RDaemon,
    config_file: String, // remember path if chdir
    config: Config,
}

impl Daemon {
    pub fn new(
        tdaemon: TDaemon,
        rdaemon: RDaemon,
        config_file: &str,
    ) -> Result<Self, Box<dyn Error>> {
        let s = fs::read_to_string(config_file)?;
        let config = Config::new(&s)?;
        let config_file = config_file.to_string();

        Ok(Self {
            tdaemon,
            rdaemon,
            config_file,
            config,
        })
    }

    pub fn run(&self) {
        for (name,task) in &self.config.tasks {
            if let Err(e) = spawn(task) {
                eprintln!("Error with task {}: {:?}", name, e);
            }
        }
        // TODO: catch Result here (propagate error)
        loop {
            let result = self.rdaemon.try_recv();

            match result {
                Ok(s) => {
                    if s == "exit" {
                        process::exit(0);
                    }
                    println!("{}", s);
                    self.tdaemon.send("Ok".to_string()).unwrap();
                },
                Err(TryRecvError::Empty) => (),
                Err(TryRecvError::Disconnected) => process::exit(1),
            }
        }
    }
}

fn redirect(path: &Path) -> std::io::Result<File> {
    let file = OpenOptions::new()
        .append(true)
        .create(true)
        .open(path)?;

    Ok(file)
}

fn spawn(task: &Task) -> Result<(), Box<dyn Error>> {
    let mut split_cmd = task.cmd.split_whitespace();
    let cmd = split_cmd.next().unwrap_or("");
 
    let stdout: Stdio = if task.stdout_logfile.is_some() {
        Stdio::from(redirect(task.stdout_logfile.as_ref().unwrap().as_path()).expect("to fix"))
    } else {
        Stdio::piped()
    };

    let stderr: Stdio = if task.stderr_logfile.is_some() {
        Stdio::from(redirect(task.stderr_logfile.as_ref().unwrap().as_path()).expect("to fix"))
    } else {
        Stdio::piped()
    };

    let begin = Instant::now();

    let output = Command::new(cmd)
        .args(split_cmd)
        .current_dir(task.chdir.as_path())
        .env_clear()
        .envs(&task.environnement)
        .stdout(stdout)
        .stderr(stderr)
        .output()?;

    println!("{}: {:?} in {:?}", task.cmd, output, begin.elapsed());
    Ok(())
}
